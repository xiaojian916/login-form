import Vue from 'vue';
import VueRouter from 'vue-router';
import LoginForm from '@/views/LoginForm.vue';
import DataTable from '@/views/DataTable.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    component: LoginForm
  },
  {
    path: '/table',
    component: DataTable
  }
];

const router = new VueRouter({
  routes
});

export default router;
